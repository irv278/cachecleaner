<?php

namespace Gtdev\CacheCleaner;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;

class BaseController extends Controller
{

    public function clearAll()
    {
        //1. Cache
        if ($this->doClearCache()) {
            return "Cache Clear failed!";
        }

        //2. Config
        if ($this->doClearConfig()) {
            return "Config Clear failed!";
        }

        //3. Route
        if ($this->doClearRoute()) {
            return "Route Clear failed!";
        }

        //4. View
        if ($this->doClearView()) {
            return "Route view failed!";
        }

        // Reoptimize Class
        if ( Artisan::call('optimize')) {
            return "Reoptimize Class failed!";
        }

        return 'Clear All Successed';
    }

    public function clearCache()
    {
        if ($this->doClearCache()) {
            return "Cache Clear failed!";
        }

        return 'Clear Cache Successed';
    }

    public function clearConfig()
    {
        if ($this->doClearConfig()) {
            return "Config Clear failed!";
        }

        return 'Clear Config Successed';
    }

    public function clearRoute()
    {
        if ($this->doClearRoute()) {
            return "Route Clear failed!";
        }

        return 'Clear Route Successed';
    }

    public function clearView()
    {
        if ($this->doClearView()) {
            return "Route view failed!";
        }

        return 'Clear View Successed';
    }

    protected function doClearCache()
    {
        $exitCode = Artisan::call('cache:clear');
        return $exitCode;
    }

    protected function doClearRoute()
    {
        $exitCode = Artisan::call('route:cache');
        return $exitCode;
    }

    protected function doClearConfig()
    {
        $exitCode = Artisan::call('config:cache');
        return $exitCode;
    }

    protected function doClearView()
    {
        $exitCode = Artisan::call('view:clear');
        return $exitCode;
    }
}
