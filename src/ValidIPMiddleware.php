<?php

namespace Gtdev\CacheCleaner;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;


class ValidIPMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $intranetList = Config::get('cleaner_allow_ip.intranetHeads');

        $whiteList = Config::get('cleaner_allow_ip.whiteList');

        $ipParse = explode(".", $request->ip());

        if (in_array($ipParse[0], $intranetList) || in_array($request->ip(), $whiteList)) {

            return $next($request);
        } else {

            return "你是誰？?";
        }
    }
}
