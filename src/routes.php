<?php

use Illuminate\Support\Facades\Route;
use Gtdev\CacheCleaner\ValidIPMiddleware;


Route::prefix('/cachecleaner')->namespace('Gtdev\CacheCleaner')->middleware(ValidIPMiddleware::class)->group(function () {

    Route::get('/all', 'BaseController@clearAll');

    Route::get('/route', 'BaseController@clearRoute');

    Route::get('/config', 'BaseController@clearConfig');

    Route::get('/cache', 'BaseController@clearCache');

    Route::get('/view', 'BaseController@clearView');
});

