<?php

namespace Gtdev\CacheCleaner;

use Illuminate\Support\ServiceProvider;

class CacheCleanerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/cleaner_allow_ip.php', 'cleaner_allow_ip'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/cleaner_allow_ip.php' => config_path('cleaner_allow_ip.php'),
        ]);

        //
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
    }
}
