# require package
## project >> composer.json

    "require": {
        ...
        "gtdev/cachecleaner": "master"
    },

    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/irv278/cachecleaner.git"
        }
    ]

## composer comment
    $ composer update
    $ composer dump-autoload -o

## publish config
    $ php artisan vendor:publish
choice Provider: Gtdev\CacheCleaner\CacheCleanerServiceProvider

# route list
- /cache-cleaner/all
- /cache-cleaner/cache
- /cache-cleaner/config
- /cache-cleaner/route
- /cache-cleaner/view
